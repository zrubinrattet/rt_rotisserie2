<?php 

class SetupTheme{
	public static function _init(){
		show_admin_bar( false );
		add_action( 'after_setup_theme', 'SetupTheme::after_setup_theme' );
		add_action( 'wp_enqueue_scripts', 'SetupTheme::wp_enqueue_scripts' );
		add_action( 'init', 'SetupTheme::init' );
		add_action( 'admin_menu', 'SetupTheme::remove_menu_pages' );
	}
	public static function init(){
		SetupTheme::clean_head();
		SetupTheme::register_section_post_type();

		register_nav_menu( 'main-nav', 'Main Navigation' );		
	}
	public static function after_setup_theme(){
		add_theme_support( 'html5' );
		add_theme_support( 'post-thumbnails' );	
	}
	public static function wp_enqueue_scripts(){
		SetupTheme::register_styles();
		SetupTheme::enqueue_styles();
		SetupTheme::register_javascript();
		SetupTheme::enqueue_javascript();	
	}
	public static function remove_menu_pages(){
		remove_menu_page( 'edit.php' );
		remove_menu_page( 'edit-comments.php' );
	}
	private static function clean_head(){
		// removes generator tag
		remove_action( 'wp_head' , 'wp_generator' );
		// removes dns pre-fetch
		remove_action( 'wp_head', 'wp_resource_hints', 2 );
		// removes weblog client link
		remove_action( 'wp_head', 'rsd_link' );
		// removes windows live writer manifest link
		remove_action( 'wp_head', 'wlwmanifest_link');	
	}
	private static function enqueue_javascript(){
		wp_enqueue_script( 'theme' );
	}
	private static function enqueue_styles(){
		wp_enqueue_style( 'theme' );
	}

	private static function register_javascript(){
		wp_register_script( 'theme', get_template_directory_uri() . '/build/js/build.js' );
	}

	private static function register_styles(){
		wp_register_style( 'theme', get_template_directory_uri() . '/build/css/build.css' );
	}
	private static function register_section_post_type(){
		$labels = array(
			'name'                => 'Sections',
			'singular_name'       => 'Section',
			'add_new'             => 'Add New Section',
			'add_new_item'        => 'Add New Section',
			'edit_item'           => 'Edit Section',
			'new_item'            => 'New Section',
			'view_item'           => 'View Section',
			'search_items'        => 'Search Sections',
			'not_found'           => 'No Sections found',
			'not_found_in_trash'  => 'No Sections found in Trash',
			'parent_item_colon'   => 'Parent Section:',
			'menu_name'           => 'Sections',
		);
		
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-grid-view',
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite'             => true,
			'capability_type'     => 'post',
			'supports'            => array(
				'title', 'page-attributes', 'post-formats', 'thumbnail',
				)
		);
		
		register_post_type( 'section', $args );

	}
}

SetupTheme::_init();

?>