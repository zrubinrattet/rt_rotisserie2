<?php 
	
	class ACFHandler{
		public static function _init(){
			add_action( 'acf/init', 'ACFHandler::init' );
			add_filter( 'acf/validate_value/key=field_ldjfo', 'ACFHandler::validate_phone_number', 10, 4 );
		}
		public static function validate_phone_number($valid, $value, $field, $input){
			// bail early if value is already invalid
			if( !$valid ) return $valid;
			// is formatted phone number or empty?
			if( is_int( (int) str_replace('-', '', $value) ) && 
				ThemeFuncs::strpos_r($value, '-') === array(3,7) &&
				strlen(str_replace('-', '', $value)) == 10 ||
				empty($value) ){
				return $valid;
			}
			else{
				return $valid = 'Phone number must be formated to 555-555-5555';
			}
		}
		public static function init(){

				
			$option_page = acf_add_options_page(array(
				'page_title' 	=> ' ',
				'menu_title' 	=> 'Theme Settings',
				'menu_slug' 	=> 'theme-settings',
			));


			acf_add_local_field_group(array(
				'key' => 'group_1',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_n11af12gafe',
						'label' => '<h1>Logo Settings</h1>',
						'type' => 'message',
					),
					array(
						'key' => 'field_n11afe',
						'label' => 'Logo (chicken)',
						'type' => 'image',
						'return_format' => 'url',
						'name' => 'logo',
						'wrapper' => array(
							'width' => '33',
						),
					),
					array(
						'key' => 'field_n113333333afe',
						'label' => 'Logo (white chicken)',
						'type' => 'image',
						'return_format' => 'url',
						'name' => 'logo_white',
						'wrapper' => array(
							'width' => '33',
						),
					),
					array(
						'key' => 'field_n112afagafe',
						'label' => 'Logo (text)',
						'type' => 'image',
						'return_format' => 'url',
						'name' => 'logo_text',
						'wrapper' => array(
							'width' => '33',
						),
					),
					array(
						'key' => 'field_n112gafe',
						'label' => '<h1>Takeout/Delivery Settings</h1>',
						'type' => 'message',
					),
					array(
						'key' => 'field_721hhfadfeagzcv',
						'label' => 'Takeout URL',
						'name' => 'takeout_url',
						'type' => 'url',
						'wrapper' => array(
							'width' => '50',
						),
					),
					array(
						'key' => 'field_721hhfagzcv',
						'label' => 'Delivery URL',
						'name' => 'delivery_url',
						'type' => 'url',
						'wrapper' => array(
							'width' => '50',
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'theme-settings',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_2',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_afq232affffdf',
						'label' => '<h1>Menu Settings</h1>',
						'type' => 'message',
					),
					array(
						'key' => 'field_f3adzc',
						'label' => 'Menu PDF',
						'name' => 'menu_pdf',
						'type' => 'file',
						'return_format' => 'url',
						'wrapper' => array(
							'width' => '50',
						),
					),
					array(
						'key' => 'field_f3af3211adzc',
						'label' => 'Menu Image',
						'name' => 'menu_image',
						'type' => 'image',
						'return_format' => 'url',
						'wrapper' => array(
							'width' => '50',
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post',
							'operator' => '==',
							'value' => '5',
						),
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'section',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_3',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_afq232adf',
						'label' => '<h1>Instagram Settings</h1>',
						'type' => 'message',
					),
					array(
						'key' => 'field_o4444hah',
						'label' => 'Instagram Site URL',
						'name' => 'instagram_url',
						'instructions' => 'Just the URL to your instagram page',
						'type' => 'url',
					),
					array(
						'key' => 'field_o0fehah',
						'label' => 'Instagram Access Token',
						'name' => 'instagram_access_token',
						'instructions' => 'Ask your developer',
						'type' => 'text',
						'wrapper' => array(
							'width' => '50',
						),
					),
					array(
						'key' => 'field_ldd33kfo',
						'label' => 'Num Posts',
						'name' => 'instagram_num_posts',
						'type' => 'select',
						'choices' => array(
							6 => 6,
							8 => 8,
						),
						'wrapper' => array(
							'width' => '50',
						),
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post',
							'operator' => '==',
							'value' => '6',
						),
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'section',
						),
					),
				),
			));

			acf_add_local_field_group(array(
				'key' => 'group_4',
				'title' => ' ',
				'fields' => array(
					array(
						'key' => 'field_afzz2adf',
						'label' => '<h1>Location Settings</h1>',
						'type' => 'message',
					),
					array(
						'key' => 'field_o01hah',
						'label' => 'Address',
						'name' => 'address',
						'type' => 'text',
					),
					array(
						'key' => 'field_ldjfo',
						'label' => 'Phone Number',
						'name' => 'phone',
						'type' => 'text',
						'instructions' => 'Use the format 555-555-5555',
						'placeholder' => '555-555-5555',
						'step' => 1,
					),
					array(
						'key' => 'field_721h3333hfagzcv',
						'label' => 'Rich Table URL',
						'name' => 'richtable_url',
						'type' => 'url',
					),
				),
				'location' => array(
					array(
						array(
							'param' => 'post',
							'operator' => '==',
							'value' => '7',
						),
						array(
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'section',
						),
					),
				),
			));

			acf_add_local_field_group(array (
				'key' => 'group_591f052542ea3',
				'title' => 'Social Media',
				'fields' => array (
					array (
						'key' => 'field_591f2b6e7bf7c',
						'label' => 'Social Media',
						'name' => 'social_media',
						'type' => 'repeater',
						'instructions' => 'Connect to Social Media:',
						'collapsed' => 'field_591f2bc17bf7e',
						'layout' => 'table',
						'sub_fields' => array (
							array (
								'key' => 'field_591f2bc17bf7e',
								'label' => 'Platform',
								'name' => 'platform',
								'type' => 'select',
								'instructions' => 'Select a social media platform:',
								'choices' => array (
									'Facebook' => 'Facebook',
									'Instagram' => 'Instagram',
									'Twitter' => 'Twitter',
								),
							),
							array (
								'key' => 'field_591f2c4b276a2',
								'label' => 'URL',
								'name' => 'url',
								'type' => 'url',
								'instructions' => 'Enter your social media profiles URL:',
								'placeholder' => 'Your URL here please!',
							),
						),
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'theme-settings',
						),
					),
				),
			));
		}
	}
	ACFHandler::_init();
?>