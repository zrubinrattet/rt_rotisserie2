<?php 

get_header(); 

$templates = array('home', 'nav');

foreach($templates as $template){
	require (locate_template("modules/" . $template . ".php" ));	
}

$the_query = new WP_Query(array(
	'post_type' => 'section',
	'posts_per_page' => -1,
	'orderby' => 'menu_order',
));

if( $the_query->have_posts() ):
	while( $the_query->have_posts() ) : $the_query->the_post();
		require (locate_template("modules/" . $post->post_name . ".php" ));		
	endwhile;
endif;

get_footer(); 

?>