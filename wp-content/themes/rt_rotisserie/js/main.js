;(function ( $, window, document, undefined ) {

	$(document).ready(function(){
		
		// this is a global component used to determine which breakpoint we're at
		// to listen for the event do $(document).on('breakpoint', eventHandlerCallback);
		// callback accepts normal amount of params but there's a custom property added to the event object called "name"
		// assuming in the callback the event object is e: console.log(e.device);
		var Breakpoint = {
			name : '',
			_init : function(){
				$(window).on('resize load', Breakpoint._resizeLoadHander);
			},
			_resizeLoadHander : function(){
				if( $(window).width() > 1024 && Breakpoint.name != 'desktop' ){
					Breakpoint.name = 'desktop';
					Breakpoint._dispatchEvent();
				}
				else if( $(window).width() <= 1024 && $(window).width() > 640 && Breakpoint.name != 'tablet' ){
					Breakpoint.name = 'tablet';	
					Breakpoint._dispatchEvent();
				}
				else if( $(window).width() < 641 && Breakpoint.name != 'mobile' ){		
					Breakpoint.name = 'mobile';		
					Breakpoint._dispatchEvent();
				}
			},
			_dispatchEvent : function(){
				$(document).trigger($.Event('breakpoint', {device: Breakpoint.name}));
			}
		}
		Breakpoint._init();

		var MobileNav = {

			toggle : $('.nav-mobile'),
			hamburger : $('.nav-mobile-hamburger'),
			nav : $('.nav'),
			menu : $('.nav-menu'),
			downArrow : $('.home-downArrow'),
			desktopNavRestingPosition: 0.8, // multiplier of $(window).height()
			navStart : undefined,
			isScrollingToNav : false,

			_init : function(){

				MobileNav._updateNavStart();

				MobileNav.navStart = MobileNav.nav.offset().top;

				MobileNav.toggle.on('click', MobileNav._clickHandler);

				MobileNav.downArrow.on('click', MobileNav._clickHandler);
				
				$(window).on('resize load scroll', MobileNav._resizeLoadScrollHandler);

				$(document).on('breakpoint', MobileNav._breakpointHandler);
			},
			_updateNavStart : function(){
				if( Breakpoint.name == 'desktop' ){
					MobileNav.navStart = $(window).height() * MobileNav.desktopNavRestingPosition;	
				}
				else{
					MobileNav.navStart = 0;		
				}
				
			},
			_breakpointHandler : function(e){
				MobileNav._updateNavStart();
			},
			_clickHandler : function(e){
				if( $(e.target).hasClass('home-downArrow') ){
					$('html, body').animate({scrollTop: $('.main').offset().top - $('.nav').height()});
				}
				else{
					if(MobileNav.nav.hasClass('nav--visible') == false){
						if( $(window).scrollTop() <= MobileNav.navStart ){
							MobileNav._scrollToNavStart();											
						}
						// if we're mobile
						if (Breakpoint.name !== 'desktop') {
							MobileNav._open();
						}
					}
					else {
						MobileNav._close();
					}
				}
			},
			_scrollToNavStart : function(){
				// you 'are scrolling' until the click event ends:
				MobileNav.isScrollingToNav = true;
				// scroll until the nav fixes to the top:
				$('html, body').animate( {
					scrollTop : MobileNav.navStart + 1
				}, 400, function(){
					MobileNav.isScrollingToNav = false;
				});
			},
			_open : function(){
				MobileNav.nav.addClass('nav--visible');
				MobileNav.hamburger.removeClass('ion-navicon-round');
				MobileNav.hamburger.addClass('ion-close');
				MobileNav.menu.slideDown();
			},
			_close : function(){
				MobileNav.nav.removeClass('nav--visible');
				MobileNav.hamburger.removeClass('ion-close');
				MobileNav.hamburger.addClass('ion-navicon-round');
				
				MobileNav.menu.slideUp(400, function(){
					// clean up inline-style after, squash bugs
					MobileNav.menu.css('display', '');
				});
			},
			_resizeLoadScrollHandler : function(e){				
				
				// auto-close nav if it's above nav start, otherwise make it sticky
				if ( $(window).scrollTop() < MobileNav.navStart ) {
					if( MobileNav.isScrollingToNav == false ){
						MobileNav.nav.removeClass('nav--sticky');
						if (MobileNav.nav.hasClass('nav--visible')){
							MobileNav._close();
						}
					}
				} else {
					MobileNav.nav.addClass('nav--sticky');
				}
				// on resize if coming from mobile the nav would be sticky on the top.
				// this removes the sticky class and resets the nav to it's starting place on desktop
				if( Breakpoint.name == 'desktop' && $(window).scrollTop() < ($(window).height() * MobileNav.desktopNavRestingPosition) ){
					MobileNav.nav.removeClass('nav--sticky');
				}
			},
		};
		MobileNav._init();


	/*
		ScrollToAnchor (to anchor)
	*/
		var ScrollToAnchor = {
			links: $('.nav-menu-item'),
			anchors: $('.scrollAnchor'),
			offset: 40,
			_init : function(){
				ScrollToAnchor.links.on('click', ScrollToAnchor._clickHandler);
			},
			_clickHandler : function(e){
				// stop # from getting appended to url
				if( $(e.target).attr('href') == '#' ) e.preventDefault();

				var linkAnchor = $(this).index();

				$('html, body').animate({
					scrollTop: $(ScrollToAnchor.anchors[linkAnchor]).offset().top - ScrollToAnchor.offset,
				});
				if (MobileNav.nav.hasClass('nav--visible')){
					MobileNav._close();
				}
			},
		}
		ScrollToAnchor._init();

	/*
		Lightbox Gallery
			See _simplelightbox.scss
			https://www.andrerinas.de/simplelightbox.html
	*/
		var lightbox = $('.instagram-featured-wrapper a').simpleLightbox({

			// options:
			// use project-relevant nav-icons (ionicons):
			closeText : '<i class="ion-close"></i>',
			navText : ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
		});

		// simple-lightbox event example;
			// see their docs
		$('.instagram-featured-wrapper a').on('error.simplelightbox', function (e) {
		  console.log(e);
		});


	/*
		End of Document Ready Function
	*/
	});
})( jQuery, window, document );

