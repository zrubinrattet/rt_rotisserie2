
<nav class="nav">
	<div class="nav-mobile">
		<i class="nav-mobile-hamburger ion-navicon-round"></i>
	</div>
	<ul class="nav-menu">
		<li class="nav-menu-item"><a href="#" class="nav-menu-item-link">Menu</a></li>
		<li class="nav-menu-item"><a target="_blank" href="<?php the_field('takeout_url', 'option'); ?>" class="nav-menu-item-link">Pick-Up</a></li>
		<li class="nav-menu-item"><a target="_blank" href="<?php the_field('delivery_url', 'option'); ?>" class="nav-menu-item-link">Delivery</a></li>
		<li class="nav-menu-item"><a href="#" class="nav-menu-item-link">Location</a></li>
	</ul>
</nav>