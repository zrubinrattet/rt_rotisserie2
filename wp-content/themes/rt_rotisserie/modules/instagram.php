<a target="_blank" href="<?php the_field('instagram_url'); ?>" class="instagram-banner scrollAnchor">Find us on Instagram</a>
<?php 
$Instagram = new Instagram(get_field('instagram_access_token'), get_field('instagram_num_posts'));
?>
<div class="instagram-featured-wrapper">
	<?php
	foreach ($Instagram::$result['data'] as $photo) :
	    $img = $photo['images'][$Instagram::$display_size];?>
        <a class="instagram-featured-tile tile-<?php the_field('instagram_num_posts'); ?>" href="<?php echo $img['url'] ?>">
            <img class="instagram-featured-tile-image" src="<?php echo $img['url'] ?>">
        </a>
	<?php endforeach; ?>
</div>