<?php 
	$bg_image = '';

	if( has_post_thumbnail( $post->ID ) ){
		$bg_image = get_the_post_thumbnail_url( $post->ID, 'full' );
	}
	else{
		$bg_image = get_template_directory_uri() . '/library/img/mobile-woodbackground.jpg';
	}
?>
<div class="main scrollAnchor" style="background-image: url('<?php echo $bg_image; ?>');">
	<div class="menu-ourmenu">
		<h1>Our Menu</h1>
	</div>

	<div class="menu">

		<a href="<?php the_field('menu_pdf'); ?>" class="menu-fullMenuPDF" target="_blank">
			<img src="<?php the_field('menu_image'); ?>">
		</a>
			
	</div>



	
	<div class="menu-button-wrapper">
		<div>	
			<a class="menu-button button-menu" target="_blank" href="<?php the_field('menu_pdf', 'option') ?>">Our Menu</a>
			<a class="menu-button button-Takeout" target="_blank" href="<?php the_field('takeout_url', 'option') ?>">Pick-Up</a>
			<a class="menu-button button-Delivery" target="_blank" href="<?php the_field('delivery_url', 'option') ?>">delivery</a>
		</div>
	</div>
</div>