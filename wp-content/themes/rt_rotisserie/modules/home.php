<div class="home">

	<div class="home-mainWrap">
		
		<div class="logo" style="background-image: url('<?php the_field('logo', 'option') ?>');"></div>

		<div class="sitetitle" style="background-image: url('<?php the_field('logo_text', 'option') ?>');"></div>
		<div class="home-mainWrap-locationinfo">
			<div class="home-mainWrap-locationinfo-address"><?php the_field('address', 7); ?></div>
			<a href="tel:<?php the_field('phone', 7); ?>" class="home-mainWrap-locationinfo-phone"><?php the_field('phone', 7); ?></a>
		</div>
		<div class="home-mainWrap-buttons">
			<a href="<?php the_field('delivery_url', 'option') ?>" class="home-mainWrap-buttons-button">Delivery</a>
			<a href="<?php the_field('takeout_url', 'option') ?>" class="home-mainWrap-buttons-button">Pick-Up</a>
		</div>

		
	</div>
	
	<div class="home-downArrow">
		<i class="ion-android-arrow-down"></i>
	</div>

</div>