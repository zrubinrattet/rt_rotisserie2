


<div class="footer-wrapper scrollAnchor">


	<div class="footer-main">

		<div class="footer-main-centerer">
		
			<div class="logo" style="background-image: url('<?php the_field('logo_white', 'option') ?>');"></div>
		
			<div class="footer-contactInfo-buttons-wrapper">
				
				<div class="footer-contactInfo">
					<h3>RT Rotisserie</h3>
					<p><?php the_field('address') ?></p>
					<p><a href="tel:<?php the_field('phone') ?>">P. <?php the_field('phone'); ?></a></p>
				</div>


				<div class="footer-buttons-wrapper">
					<a href="<?php the_field('takeout_url', 'option') ?>" class="footer-buttons">Pick-Up</a>
					<a href="<?php the_field('delivery_url', 'option') ?>" class="footer-buttons">Delivery</a>
				</div>
			</div>			
		</div>
	</div>


	<!-- end test; end home-mainWrap -->


	<div class="footer-subFooter">
	 
		 <div class="footer-linkwrapper">
			Visit our sister restaurant <a href="<?php the_field('richtable_url') ?>">Rich Table</a>
		 </div>

		 <!-- testing some stuff here is a good place: -->
		 <div class="footer-acfSocial">
		 	<?php 
 				// custom shit
 				$theFontIcon = '';

		 		if (have_rows('social_media', 'option')) :
		 			while (have_rows('social_media', 'option')) : the_row();
		 				$platform = get_sub_field('platform');
		 				$url = get_sub_field('url');

		 				// create a font-awesome class
		 				$theFontIcon = strtolower('fa-' . $platform);
			?>
						<a href="<?php echo $url; ?>"><i class="fa <?php echo $theFontIcon; ?>"></i></a>

			<?php
		 			endwhile;
		 		endif;
		 	 ?>
		 </div>

	</div>

</div>

